
Nemeroth
========

|license|_


Overview
--------

Nemeroth is a turn-based hexagonal tactical game written in Rust_.


Building
--------

``cargo build``.

TODO: add note about ``--release``!


Running
-------

``cargo run``


Android
-------

For instructions on setting up your environment see
https://github.com/tomaka/android-rs-glue#setting-up-your-environment.

TODO: ...


License
-------

Nemeroth is distributed under the terms of both
the MIT license and the Apache License (Version 2.0).

See `LICENSE-APACHE`_ and `LICENSE-MIT`_ for details.


.. |license| image:: https://img.shields.io/badge/license-MIT_or_Apache_2.0-blue.svg
.. _Rust: https://rust-lang.org
.. _LICENSE-MIT: LICENSE-MIT
.. _LICENSE-APACHE: LICENSE-APACHE
