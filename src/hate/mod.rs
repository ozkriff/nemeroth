//! Häte2d (Hate2d) is a simple 2d game engine full of _hate_.

// TODO: extract this mod to separate crate

pub mod gui;
pub mod geom;
pub mod screen;
pub mod fs;

mod texture;
mod event;
mod mesh;
mod text;
mod pipeline;
mod visualizer;
mod screen_stack;
mod time;
mod sprite;
mod context;
mod settings;

pub use hate::settings::Settings;
pub use hate::visualizer::Visualizer;
pub use hate::sprite::Sprite;
pub use hate::screen::Screen;
pub use hate::context::Context;
pub use hate::time::Time;
pub use hate::event::Event;
pub use hate::scene::{ActionScene};

// TODO: move to separate files
pub mod scene {
    use std::collections::HashMap;
    use hate::{Sprite, Context, Time};

    pub use hate::scene::action::Action;

    /// Scene Node ID
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Id(i32);

    #[derive(Debug)]
    pub struct Scene {
        hidden: HashMap<Id, Sprite>,
        sprites: HashMap<Id, Sprite>,
        next_id: Id,
    }

    impl Scene {
        pub fn new() -> Self {
            Self {
                sprites: HashMap::new(),
                hidden: HashMap::new(),
                next_id: Id(0),
            }
        }

        fn alloc_id(&mut self) -> Id {
            let id = self.next_id;
            self.next_id.0 += 1;
            id
        }

        pub fn add(&mut self, sprite: Sprite) -> Id {
            let id = self.alloc_id();
            self.hidden.insert(id, sprite);
            id
        }

        pub fn show(&mut self, id: Id) {
            let sprite = self.hidden.remove(&id).unwrap();
            self.sprites.insert(id, sprite);
        }

        pub fn draw(&self, context: &mut Context) {
            let projection_matrix = context.projection_matrix();
            for sprite in self.sprites.values() {
                sprite.draw(context, projection_matrix);
            }
        }
    }

    #[derive(Debug)]
    struct ActionInterpreter {
        actions: Vec<Box<Action>>,
    }

    impl ActionInterpreter {
        pub fn new() -> Self {
            Self { actions: Vec::new() }
        }

        pub fn add(&mut self, scene: &mut Scene, mut action: Box<Action>) {
            action.begin(scene);
            self.actions.push(action);
        }

        pub fn tick(&mut self, scene: &mut Scene, dtime: Time) {
            let mut forked_actions = Vec::new();
            for action in &mut self.actions {
                action.update(scene, dtime);
                if let Some(forked_action) = action.try_fork(scene) {
                    forked_actions.push(forked_action);
                }
                if action.is_finished() {
                    action.end(scene);
                }
            }
            for action in forked_actions {
                self.add(scene, action);
            }
            self.actions.retain(|action| !action.is_finished());
        }
    }

    #[derive(Debug)]
    pub struct ActionScene {
        scene: Scene,
        interpreter: ActionInterpreter,
    }

    impl ActionScene {
        pub fn new() -> Self {
            Self {
                scene: Scene::new(),
                interpreter: ActionInterpreter::new(),
            }
        }

        pub fn add_action(&mut self, action: Box<Action>) {
            self.interpreter.add(&mut self.scene, action);
        }

        pub fn add_sprite(&mut self, sprite: Sprite) -> Id {
            self.scene.add(sprite)
        }

        pub fn tick(&mut self, dtime: Time) {
            self.interpreter.tick(&mut self.scene, dtime);
        }

        pub fn draw(&self, context: &mut Context) {
            self.scene.draw(context);
        }
    }

    pub mod action {
        use std::fmt::Debug;
        use hate::Time;
        use hate::scene::Scene;

        pub use hate::scene::action::sequence::Sequence;
        pub use hate::scene::action::show::Show;
        pub use hate::scene::action::move_by::MoveBy;
        pub use hate::scene::action::fork::Fork;
        pub use hate::scene::action::sleep::Sleep;

        pub trait Action: Debug {
            fn begin(&mut self, _scene: &mut Scene) {}
            fn update(&mut self, _scene: &mut Scene, _dtime: Time) {}
            fn end(&mut self, _scene: &mut Scene) {}

            fn try_fork(&mut self, _scene: &mut Scene) -> Option<Box<Action>> {
                None
            }

            fn is_finished(&self) -> bool {
                true
            }
        }

        mod sequence {
            use std::collections::VecDeque;
            use hate::Time;
            use hate::scene::{Action, Scene};

            #[derive(Debug)]
            pub struct Sequence {
                actions: VecDeque<Box<Action>>,
            }

            impl Sequence {
                pub fn new(actions: Vec<Box<Action>>) -> Self {
                    Self { actions: actions.into() }
                }

                /// Current action
                fn action(&mut self) -> &mut Action {
                    &mut **self.actions.front_mut().unwrap()
                }

                fn end_current_action_and_start_next(&mut self, scene: &mut Scene) {
                    assert!(!self.actions.is_empty());
                    assert!(self.action().is_finished());
                    self.action().end(scene);
                    self.actions.pop_front().unwrap();
                    if !self.actions.is_empty() {
                        self.action().begin(scene);
                    }
                }
            }

            impl Action for Sequence {
                fn begin(&mut self, scene: &mut Scene) {
                    if !self.actions.is_empty() {
                        self.action().begin(scene);
                    }
                }

                fn update(&mut self, scene: &mut Scene, dtime: Time) {
                    if self.actions.is_empty() {
                        return;
                    }
                    self.action().update(scene, dtime);
                    // Skipping instant actions
                    while !self.actions.is_empty() && self.action().is_finished() {
                        self.end_current_action_and_start_next(scene);
                    }
                }

                fn end(&mut self, _: &mut Scene) {
                    assert!(self.actions.is_empty());
                }

                fn is_finished(&self) -> bool {
                    self.actions.is_empty()
                }

                fn try_fork(&mut self, scene: &mut Scene) -> Option<Box<Action>> {
                    if self.actions.is_empty() {
                        return None;
                    }
                    let forked_action = self.action().try_fork(scene);
                    if forked_action.is_some() && self.action().is_finished() {
                        self.end_current_action_and_start_next(scene);
                    }
                    forked_action
                }
            }
        }

        mod fork {
            use hate::scene::{Scene, Action};

            #[derive(Debug)]
            pub struct Fork {
                action: Option<Box<Action>>,
            }

            impl Fork {
                pub fn new(action: Box<Action>) -> Self {
                    Self { action: Some(action) }
                }
            }

            impl Action for Fork {
                fn try_fork(&mut self, _: &mut Scene) -> Option<Box<Action>> {
                    self.action.take()
                }

                fn is_finished(&self) -> bool {
                    self.action.is_none()
                }

                fn end(&mut self, _: &mut Scene) {
                    assert!(self.action.is_none());
                }
            }
        }

        mod show {
            use hate::scene::{Id, Scene, Action};

            #[derive(Debug)]
            pub struct Show {
                id: Id,
            }

            impl Show {
                pub fn new(id: Id) -> Self {
                    Self { id }
                }
            }

            impl Action for Show {
                fn begin(&mut self, scene: &mut Scene) {
                    scene.show(self.id);
                }
            }
        }

        mod move_by {
            use hate::Time;
            use hate::scene::{self, Scene, Action};
            use hate::geom::Point;

            #[derive(Debug)]
            pub struct MoveBy {
                id: scene::Id,
                duration: Time,
                delta: Point,
                progress: Time,
            }

            impl MoveBy {
                pub fn new(id: scene::Id, delta: Point, duration: Time) -> Self {
                    Self {
                        id,
                        delta,
                        duration,
                        progress: Time(0.0),
                    }
                }
            }

            impl Action for MoveBy {
                fn update(&mut self, scene: &mut Scene, mut dtime: Time) {
                    let mut sprite = scene.sprites.get_mut(&self.id).unwrap();
                    let old_pos = sprite.pos();
                    if dtime.0 + self.progress.0 > self.duration.0 {
                        dtime = Time(self.duration.0 - self.progress.0);
                    }
                    let new_pos = Point(old_pos.0 + dtime.0 * self.delta.0 / self.duration.0);
                    sprite.set_pos(new_pos);
                    self.progress.0 += dtime.0;
                }

                fn is_finished(&self) -> bool {
                    let eps = 0.00001;
                    self.progress.0 > (self.duration.0 - eps)
                }
            }
        }

        mod sleep {
            use hate::Time;
            use hate::scene::{Scene, Action};

            #[derive(Debug)]
            pub struct Sleep {
                duration: Time,
                time: Time,
            }

            impl Sleep {
                pub fn new(duration: Time) -> Self {
                    Self {
                        duration: duration,
                        time: Time(0.0),
                    }
                }
            }

            impl Action for Sleep {
                fn is_finished(&self) -> bool {
                    self.time.0 / self.duration.0 > 1.0
                }

                fn update(&mut self, _: &mut Scene, dtime: Time) {
                    self.time.0 += dtime.0;
                }
            }
        }

        // TODO: change size
        // TODO: change color
        // TODO: change rotation
        // TODO: Easing
    }
}
