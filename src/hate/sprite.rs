use std::path::Path;
use cgmath::{Matrix4, Vector2, Zero};
use hate::Context;
use hate::mesh::RMesh;
use hate::geom::{self, Size, Point};

#[derive(Debug, Clone)]
pub struct Sprite {
    mesh: RMesh,
    pos: Point,
}

impl Sprite {
    pub fn from_path<P: AsRef<Path>>(context: &mut Context, path: P, size: f32) -> Self {
        let size = Size { w: size, h: size };
        Sprite::from_path_rect(context, path, size)
    }

    pub fn from_path_rect<P: AsRef<Path>>(context: &mut Context, path: P, size: Size<f32>) -> Self {
        let texture = context.load_texture(path);
        let mesh = RMesh::new(context, texture, size);
        let pos = Point(Vector2::zero());
        Self { mesh, pos }
    }

    pub fn from_mesh(mesh: RMesh) -> Self {
        let pos = Point(Vector2::zero());
        Self { mesh, pos }
    }

    pub fn pos(&self) -> Point {
        self.pos
    }

    pub fn set_pos(&mut self, pos: Point) {
        self.pos = pos;
    }

    pub fn size(&self) -> Size<f32> {
        self.mesh.size()
    }

    pub fn draw(&self, context: &mut Context, parent_matrix: Matrix4<f32>) {
        let model_matrix = geom::pos_to_matrix(self.pos);
        context.draw_mesh(parent_matrix * model_matrix, self.mesh.mesh());
    }
}
