#[cfg(target_os = "android")]
extern crate android_glue;

#[macro_use]
extern crate gfx;

#[macro_use]
extern crate serde_derive;

extern crate gfx_window_glutin;
extern crate gfx_device_gl;
extern crate rand;
extern crate cgmath;
extern crate glutin;
extern crate png;
extern crate rusttype;
extern crate serde;
extern crate toml;

pub mod hate;
mod screen;

pub fn main() {
    enable_backtrace();
    let settings = toml::from_slice(&hate::fs::load("settings.toml")).unwrap();
    let mut visualizer = hate::Visualizer::new(settings);
    let start_screen = Box::new(screen::MainMenu::new(visualizer.context_mut()));
    visualizer.run(start_screen);
}

fn enable_backtrace() {
    if std::env::var("RUST_BACKTRACE").is_err() {
        std::env::set_var("RUST_BACKTRACE", "1");
    }
}
