use cgmath::vec2;
use hate::{self, Time, Sprite, Event, Screen, Context, ActionScene};
use hate::geom::Point;
use hate::gui::{self, Gui};
use hate::scene::action;

#[derive(Copy, Clone, Debug)]
enum Command {
    A,
    B,
    C,
    D,
    E,
    F,
    Exit,
}

#[derive(Debug)]
pub struct Game {
    scene: ActionScene,
    gui: Gui<Command>,
    button_f_id: gui::Id,
}

impl Game {
    pub fn new(context: &mut Context) -> Self {
        let mut scene = ActionScene::new();

        let test_sprite_1 = Sprite::from_path(context, "test_sprite.png", 0.3);
        let test_id_1 = scene.add_sprite(test_sprite_1);

        let mut test_sprite_2 = Sprite::from_path(context, "test_sprite.png", 0.2);
        test_sprite_2.set_pos(Point(vec2(0.25, 0.25)));
        let test_id_2 = scene.add_sprite(test_sprite_2);

        let mut test_sprite_3 = Sprite::from_path(context, "test_sprite.png", 0.3);
        test_sprite_3.set_pos(Point(vec2(0.5, 0.5)));
        let test_id_3 = scene.add_sprite(test_sprite_3);

        let test_action = Box::new(action::Sequence::new(vec![
            Box::new(action::Show::new(test_id_1)),
            Box::new(
                action::Fork::new(Box::new(action::Sequence::new(vec![
                    Box::new(action::MoveBy::new(
                        test_id_1,
                        Point(vec2(-0.5, -0.5)),
                        Time(1.0),
                    )),
                    Box::new(action::MoveBy::new(
                        test_id_1,
                        Point(vec2(1.0, 0.0)),
                        Time(0.5),
                    )),
                    Box::new(action::MoveBy::new(
                        test_id_1,
                        Point(vec2(0.0, 1.0)),
                        Time(0.5),
                    )),
                ])))
            ),
            Box::new(action::Sleep::new(Time(0.5))),
            Box::new(action::Show::new(test_id_2)),
            Box::new(
                action::Fork::new(Box::new(action::Sequence::new(vec![
                    Box::new(action::MoveBy::new(
                        test_id_2,
                        Point(vec2(-0.5, 0.0)),
                        Time(0.25),
                    )),
                    Box::new(action::MoveBy::new(
                        test_id_2,
                        Point(vec2(0.5, 0.0)),
                        Time(0.25),
                    )),
                    Box::new(action::MoveBy::new(
                        test_id_2,
                        Point(vec2(-0.5, 0.0)),
                        Time(0.25),
                    )),
                    Box::new(action::MoveBy::new(
                        test_id_2,
                        Point(vec2(0.5, 0.0)),
                        Time(0.25),
                    )),
                ])))
            ),
            Box::new(action::Sleep::new(Time(0.5))),
            Box::new(action::Show::new(test_id_3)),
        ]));
        scene.add_action(test_action);

        let mut gui = Gui::new(context);

        let _ /*layout_a_id*/ = {
            let sprite_a = gui::text_sprite(context, "A", 0.1);
            let sprite_b = gui::text_sprite(context, "B", 0.1);
            // let sprite_b = Sprite::from_path(context, "test_sprite.png");
            let sprite_c = gui::text_sprite(context, "C", 0.1);
            let sprite_a_id = gui.add_button(context, sprite_a, Command::A);
            let sprite_b_id = gui.add_button(context, sprite_b, Command::B);
            let sprite_c_id = gui.add_button(context, sprite_c, Command::C);
            let anchor = gui::Anchor {
                vertical: gui::VAnchor::Top,
                horizontal: gui::HAnchor::Left,
            };
            let direction = gui::Direction::Right;
            gui.add_layout(anchor, direction, vec![
                sprite_a_id,
                sprite_b_id,
                sprite_c_id,
            ])
        };

        let button_f_id;
        let _ /*layout_b_id*/ = {
            let sprite_d = gui::text_sprite(context, "D", 0.1);
            let sprite_e = gui::text_sprite(context, "E", 0.1);
            let sprite_f = gui::text_sprite(context, "F", 0.1);
            let sprite_d_id = gui.add_button(context, sprite_d, Command::D);
            let sprite_e_id = gui.add_button(context, sprite_e, Command::E);
            let sprite_f_id = gui.add_button(context, sprite_f, Command::F);
            button_f_id = sprite_f_id;
            let anchor = gui::Anchor {
                vertical: gui::VAnchor::Bottom,
                horizontal: gui::HAnchor::Right,
            };
            let direction = gui::Direction::Up;
            gui.add_layout(anchor, direction, vec![
                sprite_d_id,
                sprite_e_id,
                // layout_a_id, // TODO: nested layouts
                sprite_f_id,
            ])
        };

        let _ /*layout_c_id*/ = {
            let sprite_a = gui::text_sprite(context, "move: A", 0.1);
            let sprite_b = gui::text_sprite(context, "attack: B", 0.1);
            let sprite_exit = gui::text_sprite(context, "exit", 0.1);
            let sprite_a_id = gui.add_button(context, sprite_a, Command::A);
            let sprite_b_id = gui.add_button(context, sprite_b, Command::B);
            let sprite_id_exit = gui.add_button(context, sprite_exit, Command::Exit);
            let anchor = gui::Anchor {
                vertical: gui::VAnchor::Middle,
                horizontal: gui::HAnchor::Left,
            };
            let direction = gui::Direction::Up;
            gui.add_layout(anchor, direction, vec![
                sprite_a_id,
                sprite_b_id,
                sprite_id_exit,
            ])
        };

        Self {
            scene,
            gui,
            button_f_id,
        }
    }

    fn exit(&mut self, context: &mut Context) {
        context.add_command(hate::screen::Command::Pop);
    }

    fn handle_commands(&mut self, context: &mut Context) {
        while let Some(command) = self.gui.try_recv() {
            match command {
                Command::A => println!("A"),
                Command::B => println!("B"),
                Command::C => println!("C"),
                Command::D => println!("D"),
                Command::E => println!("E"),
                Command::F => {
                    println!("F");
                    let new_sprite = gui::text_sprite(context, "FF", 0.1);
                    self.gui.update_sprite(
                        context,
                        self.button_f_id,
                        new_sprite,
                    );
                }
                Command::Exit => self.exit(context),
            }
        }
    }
}

impl Screen for Game {
    fn tick(&mut self, context: &mut Context, dtime: Time) {
        context.set_basic_color([1.0, 1.0, 1.0, 1.0]);
        self.scene.tick(dtime);
        self.scene.draw(context);
        self.gui.draw(context);
    }

    fn handle_event(&mut self, context: &mut Context, event: Event) {
        match event {
            Event::Click { pos } => self.gui.click(pos),
            Event::Resize { aspect_ratio } => self.gui.resize(aspect_ratio),
        }
        self.handle_commands(context);
    }
}
